#!/usr/bin/env node
var hg = require('./client/stdlib.js'),
	key = process.argv[2] || '',
	conf = hg.conf
	;

if (/^-h|--help$/.test(key)) {
	console.log('Usage: ' + process.argv.slice(0, 2).join(' ') + ' [key]\n');
	console.log('Prints the given configuration key, or the whole configuration if the key is omitted.\n');
	console.log('Deep keys can be specified by using a dot (.), eg: `' + process.argv[1] + ' mysql.host`\n');
	console.log('If the given key is scalar it is printed without formatting or quoting. If it is a deep structure it is printed JSON-encoded printed.\n');
	process.exit();
}

key.split('.').forEach(function(k) {
	if (!conf[k]) {
		conf = '';
		return false;
	}
	conf = conf[k];
});
console.log(typeof conf == 'string' ? conf : JSON.stringify(conf));
