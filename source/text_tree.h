/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_TEXT_TREE_H
#define HUBGRAPH_TEXT_TREE_H

#include <iostream>
/**
 * For debugging purposes it is nice to have the ordering of these trees defined,
 * so that you can send two trees to a stream and then get a meaningful diff of
 * them.
 *
 * There's no semantic reason during the normal run of things to care about the 
 * ordering, though.
 */ 
#ifdef ORDER_TEXT_TREE
	#include <map>
	#define TTMap std::map
#else
	#include <unordered_map>
	#define TTMap std::unordered_map
#endif
#include <string>
#include <list>
#include <fstream>

namespace HubGraph 
{

class Stemmer;

/**
 * Store text in a tree where each letter leads to its continuation, and each
 * letter that ends a word has a weight equal to the number of times that word
 * has been entered into the tree.
 *
 * For example:
 *
 * TextNode<char> tree;
 * tree.add("base");
 * tree.add("base");
 * tree.add("baseball");
 * tree.add("basketball");
 * tree.add("sports");
 * std::cout << tree;
 *
 * Looks like:
 * s[0]: 
 * --p[0]: 
 * ----o[0]: 
 * ------r[0]: 
 * --------t[0]: 
 * ----------s[1]: 
 * b[0]: 
 * --a[0]: 
 * ----s[0]: 
 * ------e[2]: 
 * --------b[0]: 
 * ----------a[0]: 
 * ------------l[0]: 
 * --------------l[1]: 
 * ------k[0]: 
 * --------e[0]: 
 * ----------t[0]: 
 * ------------b[0]: 
 * --------------a[0]: 
 * ----------------l[0]: 
 * ------------------l[1]:
 *
 * Single-child nodes are not compacted.
 * 
 * A tree like this can be used to suggest partial-word completions given a 
 * corpus.
 *
 * traverse()-ing the tree returns the subtree after the finding the end 
 * position of the supplied string:
 *
 * std::cout << *tree.traverse("bas");
 *
 * yields:
 *
 * ------e[2]: 
 * --------b[0]: 
 * ----------a[0]: 
 * ------------l[0]: 
 * --------------l[1]: 
 * ------k[0]: 
 * --------e[0]: 
 * ----------t[0]: 
 * ------------b[0]: 
 * --------------a[0]: 
 * ----------------l[0]: 
 * ------------------l[1]:
 *
 * Trees can be converted to a list of the strings they represent ordered by
 * their liklihood given the corpus (and then alphabetically to break ties) 
 * with toStrings():
 *
 * std::list<std::string> suggestions = tree.traverse("bas")->toStrings();
 * std::copy(
 * 	suggestions.begin(), 
 * 	suggestions.end(), 
 * 	std::ostream_iterator<std::string>(std::cout, "\n")
 * );
 *
 * yields:
 *
 * e
 * eball
 * ketball
 *
 * Prepending the supplied string yields full suggestions.
 *
 * @tparam charT char or wchar_t depending on whether you are using Unicode
 */
template <typename charT>
class TextNode
{
	/**
	 * Print ASCII trees
	 */
	friend std::ostream&  operator<<(std::ostream& os,  const TextNode<char>& node);
	/**
	 * Print Unicode trees
	 */
	friend std::wostream& operator<<(std::wostream& os, const TextNode<wchar_t>& node);

	/**
	 * Hashtable from character -> node
	 */
	typedef TTMap<charT, TextNode<charT>* > Tree;

public:
	/**
	 * A terminal string coupled with its weight
	 */
	typedef std::pair<size_t, std::basic_string<charT> > WeightedString;
	/**
	 * A list of strings coupled with their weight.
	 * Intermediary form used to get a sorted list of strings without their
	 * weights attached explicitly.
	 */
	typedef std::list<WeightedString> WeightedStringList;
	/**
	 * Create an empty node
	 */
	TextNode(const size_t depth = 1);
	/**
	 * Create a node and fill it given a string
	 */
	TextNode(const std::basic_string<charT>& str, const size_t depth = 1);
	/**
	 * Delete subnodes
	 */
	~TextNode();
	/**
	 * Add a string beginning at this node
	 */
	void add(const std::basic_string<charT>& str);
	/**
	 * Remove a string beginning at this node
	 */ 
	void remove(const std::basic_string<charT>& str);
	/**
	 * Follow the supplied string to its position in the tree, and return that 
	 * subtree
	 */
//	TextNode<charT>* traverse(const std::basic_string<charT>& str) const;
	TextNode<char>* traverse(const std::basic_string<char>& str) const;
	/**
	 * Convert tree to a list of strings sorted by their liklihood of occuring
	 * in the tree's input data (and then alphabetically for ties)
	 */
	std::list<std::pair<size_t, std::basic_string<charT> > > toStrings(size_t limit, size_t threshold , Stemmer* stemmer) const;

	size_t getWeight() const { return weight; }
	
	bool isEmpty() const { return tree.size() == 0; }

	void decreaseWeight() {
		if (weight > 0) {
			--weight;
		}
	}

	void write(std::ofstream& fh) const;
	void read(std::ifstream& fh, size_t depth = 1);
	
	Tree getTree() { return tree; }
	/**
	 * Sort a list of strings coupled to their weights by their weight, and then
	 * alphabetically to break ties
	 */
	static bool WeightedStringCmp(WeightedString a, WeightedString b);


private:
	TextNode(const TextNode&);
	WeightedStringList _toStrings() const;
	Tree tree;
	size_t weight, depth;
};

template <typename charT>
class TextTree
{
public:
	TextTree() : filename(""), tree(nullptr) {}
	~TextTree();
	void read(const std::string& filename);
	TextNode<charT>& getRoot() { return *tree; }
private:
	TextTree(const TextTree<charT>&) = delete;
	TextTree<charT>& operator=(const TextTree<charT>&) = delete;
	std::string filename;
	TextNode<charT>* tree;
};

typedef TextTree<char> AsciiTextTree;
//typedef TextTree<wchar_t> UnicodeTextTree;

typedef TextNode<char> AsciiTextNode;
//typedef TextNode<wchar_t> UnicodeTextNode;
}

#endif
