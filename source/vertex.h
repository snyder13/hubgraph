/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_VERTEX_H
#define HUBGRAPH_VERTEX_H

#include <list>
#include <set>
#include <string>

#include <boost/algorithm/string/regex.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "database.h"
#include "edge.h"
#include "text.h"

#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <v8.h>
#pragma GCC diagnostic warning "-Wunused-parameter"

namespace HubGraph {
class Vertex;
}

namespace boost { 
	
namespace serialization {

template<class Archive>
auto serialize(Archive& ar, HubGraph::Vertex& v, const unsigned int) -> void;

}

}
namespace HubGraph {

typedef Database<Vertex, Edge> HgDatabase;

class Vertex 
{
	template<class Archive>
	friend auto boost::serialization::serialize(Archive& ar, HubGraph::Vertex& v, const unsigned int) -> void;

	typedef std::list<std::pair<size_t, std::string> > WeightedStrings;
public:
	Vertex() {}
	Vertex(v8::Handle<v8::Object> props);
	Vertex(const std::string& props);

	auto aerate() -> void;
	auto remove(HgDatabase* db) -> void;

	auto setProperties(const std::string& props) -> void;
	auto setProperties(v8::Handle<v8::Object> props) -> void;
	auto internText(HgDatabase* db = nullptr) -> void;

	auto getProperties() const -> const std::string& { return properties; }
	auto getDomain()     const -> const std::string& { return domain; }
	auto getId()         const -> const std::string& { return id; }

	static auto getSpellingSuggestions(std::string base, size_t limit = 1, size_t editDistance = 2) -> WeightedStrings;
	static auto completeTerm(const std::string& term, size_t limit = 20, size_t threshold = 0) -> WeightedStrings; 



	std::string qualifiedId;
private:
	static auto canonicalizeWords(std::string text) -> std::vector<std::string>;
	static auto getEdits(const std::string& base, std::insert_iterator<std::set<std::string> >& out) -> void;
	
	auto parseText(HgDatabase* db, const std::string& text, bool inTitle) -> void;
	auto unparseText(HgDatabase* db, const std::string& text) -> void;

	std::string properties, domain, id;
	static const v8::Persistent<v8::String> bodyV8Sym;
	static const v8::Persistent<v8::String> titleV8Sym;
	static const boost::regex irrelevantChars;
	static const boost::regex whiteSpace;
};

}

namespace boost { 
	
namespace serialization {

template<class Archive>
auto serialize(Archive& ar, HubGraph::Vertex& v, const unsigned int) -> void {
	ar & v.properties;
}

} 

}

#endif
