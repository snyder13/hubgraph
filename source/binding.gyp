{
	'targets': [{
		'target_name': 'hubgraph',
		'include_dirs': [
			'vendor/node/deps/v8/include',
			'vendor/node/stage/include',
			'vendor/libstemmer_c/include'
		],
		'libraries': [
			'<!(pwd)/vendor/libstemmer_c/libstemmer.a',
			'-lboost_serialization', 
			'-lboost_regex', 
			'-lboost_system', 
			'-licui18n', 
			'-licuuc'
		],
		'cflags_cc!': [ 
			'-fno-rtti',
			'-fno-exceptions'
		],
		'cflags+': [
			'-std=<!((cc -std=c++11 -x c++ -E -v - </dev/null 2>/dev/null >/dev/null && echo c++11) || (cc -std=c++0x -x c++ -E -v - </dev/null 2>/dev/null >/dev/null && echo c++0x))', 
			'-Wall', 
			'-Wextra', 
			'-fno-strict-aliasing', 
			'-fPIC',
			'-Dnullptr=0'
		],
		'sources': [
			'api.cc',
			'database.cc',
			'edge.cc',
			'text.cc',
			'text_tree.cc',
			'vertex.cc'
		]
	}]
}
