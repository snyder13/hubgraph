/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 * This file demonstrates how to use hubgraph to do graph-y stuff. Please 
 * direct any questions or suggestions to the issue tracker located where
 * you downloaded this file.
 */

// Step 1: get a reference to the hubgraph module
// 	Most directly, you can say var hg = require('hubgraph'), which provides
// 	you access to the C++ API. This API is augmented with some simple 
// 	JavaScript utilities you can get by accessing hubgraph through stdlib.js:
var hg = require('./stdlib.js');
console.log(hg);
