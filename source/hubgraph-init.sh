#!/bin/sh

### BEGIN INIT INFO
# Provides:		hubgraph
# Required-Start:	$remote_fs $syslog
# Required-Stop:	$remote_fs $syslog
# Default-Start:	2 3 4 5
# Default-Stop:	0 1 6	
# Short-Description: Hubgraph Database Server	
### END INIT INFO

set -e

. /lib/lsb/init-functions

ROOT=/
SCRIPT="${ROOT}var/hubgraph/client/hubzero/server.js"
FOREVER="${ROOT}var/hubgraph/node_modules/forever/bin/forever"

case "$1" in
  start | stop | restart)
	su -c "${FOREVER} $1 ${SCRIPT}" hubgraph
  ;;
  *)
	log_action_msg "Usage: $0 {start|stop|restart}"
	exit 1
esac

exit 0
