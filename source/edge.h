/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_EDGE_H
#define HUBGRAPH_EDGE_H

#include <string>

namespace HubGraph {
class Edge;
}

namespace boost { namespace serialization {

template<typename Archive>
auto serialize(Archive& ar, HubGraph::Edge& e, const unsigned int) -> void;

} }

namespace HubGraph {

class Edge 
{
	template<typename Archive>
	friend auto boost::serialization::serialize(Archive&, HubGraph::Edge&, const unsigned int) -> void;
public:
	Edge() : type(""), weight(0) {}
	Edge(const std::string& type, double weight = 1.0) : type(type), weight(weight) {
	}
	auto incrementWeight(double weight) -> void { this->weight += weight; }
	auto getType() const -> const std::string& { return type; }
	auto getWeight() const -> double { return weight; }
private:
	std::string type;
	double weight;
};

}

namespace boost { namespace serialization {

template<typename Archive>
auto serialize(Archive& ar, HubGraph::Edge& e, const unsigned int) -> void {
	ar & e.type;
	ar & e.weight;
}

} }




#endif
