/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#include "text.h"
#include <map>
#include <boost/assign.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/foreach.hpp>

namespace HubGraph {

sb_stemmer* Stemmer::engStem = sb_stemmer_new("english", "ISO_8859_1");

auto Stemmer::operator()(const std::string& str) -> std::string {
	static const boost::regex latinEndings("((^['\"]+|(a|um|'|\")+$))");
	std::string rv = boost::regex_replace(
		std::string(reinterpret_cast<const char*>(sb_stemmer_stem(engStem, reinterpret_cast<const sb_symbol*>(str.c_str()), str.length()))),
		latinEndings,
		"",
		boost::match_default|boost::format_all
	);
	++reverse[rv][str];
	return rv;
}

auto Stemmer::unstem(const std::string& str) const -> std::string {
	static  const boost::regex splitWords("[^-_'/\\\\\\w]+");
	std::vector<std::string> words;
	boost::algorithm::split_regex(words, str, splitWords);
	auto rv = unstem(words);
	return rv == "" ? str : rv;
}

auto Stemmer::unstem(const std::vector<std::string>& words) const -> std::string {
	std::string out;
	bool first = true;
	BOOST_FOREACH(const auto& word, words) {
		auto it = reverse.find(word);
		if (it != reverse.end()) {
			size_t max = 0;
			std::string found;
			BOOST_FOREACH(const auto& pair, it->second) {
				if (pair.second > max) {
					found = pair.first;
					max = pair.second;
				}
			}
			out += (first ? "" : " ") + (found == "" ? word : found);
			first = false;
		}
	}
	return out;
}

Stemmer Text::stem = Stemmer();
AsciiTextNode* Text::tree = new AsciiTextNode;

const std::map<const std::string, bool> Text::stopWords = boost::assign::map_list_of 
	// started with a list online, several added since as things popped up. regretfully out of order now
	// also, "indeed" was *removed* because NEES has a tool called that. smh
	("",        true) ("a",       true) ("able",      true) ("about",   true) ("across",   true) 
	("after",   true) ("akin",    true) ("all",       true) ("almost",  true) ("also",     true) 
	("am",      true) ("among",   true) ("an",        true) ("and",     true) ("any",      true) 
	("are",     true) ("aren't",  true) ("as",        true) ("at",      true) ("be",       true) 
	("because", true) ("been",    true) ("between",   true) ("but",     true) ("by",       true) 
	("can",     true) ("cannot",  true) ("could",     true) ("dear",    true) ("did",      true) 
	("do",      true) ("does",    true) ("don't",     true) ("either",  true) ("else",     true) 
	("ever",    true) ("every",   true) ("for",       true) ("from",    true) ("get",      true) 
	("got",     true) ("had",     true) ("has",       true) ("have",    true) ("he",       true) 
	("her",     true) ("here",    true) ("hers",      true) ("him",     true) ("his",      true) 
	("how",     true) ("however", true) ("i",         true) ("if",      true) ("in",       true) 
	("into",    true) ("is",      true) ("isn't",     true) ("it",      true) ("its",      true)     
	("just",    true) ("least",   true) ("let",       true) ("like",    true) ("likely",   true)  
	("may",     true) ("me",      true) ("might",     true) ("more",    true) ("most",     true) 
	("must",    true) ("my",      true) ("neither",   true) ("no",      true) ("nor",      true) 
	("not",     true) ("of",      true) ("off",       true) ("often",   true) ("on",       true) 
	("once",    true) ("only",    true) ("or",        true) ("other",   true) ("our",      true) 
	("own",     true) ("rather",  true) ("said",      true) ("say",     true) ("says",     true) 
	("she",     true) ("should",  true) ("since",     true) ("so",      true) ("some",     true) 
	("than",    true) ("that",    true) ("the",       true) ("their",   true) ("them",     true) 
	("then",    true) ("there",   true) ("therefore", true) ("these",   true) ("they",     true) 
	("this",    true) ("those",   true) ("though",    true) ("through", true) ("tis",      true) 
	("to",      true) ("too",     true) ("twas",      true) ("twice",   true) ("us",       true) 
	("wants",   true) ("was",     true) ("we",        true) ("were",    true) ("what",     true) 
	("when",    true) ("where",   true) ("which",     true) ("while",   true) ("who",      true)
	("whoever", true) ("whom",    true) ("whomever",  true) ("why",     true) ("will",     true) 
	("with",    true) ("would",   true) ("yet",       true) ("you",     true) ("your",     true) 
	("you're",  true) ("you've",  true) ("one",       true) ("two",     true) ("three",    true) 
	("four",    true) ("five",    true) ("six",       true) ("seven",   true) ("eight",    true) 
	("nine",    true) ("ten",     true) ("now",       true) ("came",    true) ("come",     true)
	("well",    true) ("again",   true) ("still",     true) ("ing",     true) ("himself",  true)
	("herself", true) ("itself",  true) ("wouldnt",   true) ("couldnt", true) ("shouldnt", true)
	("theyre",  true) ("theres",  true) ("heres",     true) ("wheres",  true) ("use",      true)
	("dan",     true) ("san",     true) ("dans",      true) ("sur",     true) ("qui",      true)
	("non",     true) ("nous",    true) ("such",      true) ("very",    true) ("veri",     true) 
	("many",    true) ("mani",    true) ("any",       true) ("ani",     true) ("org",      true) 
	("com",     true) ("edu",     true) ("info",      true) ("thus",    true) ("without",  true) 
	("therefor", true) ("shall",  true) ("casis",     true) ("nbsp",    true) ("until",    true) 
	;

auto Text::isStopWord(const std::string& word) -> bool {
	return word.length() == 1 || stopWords.find(word) != stopWords.end();
}

// ty http://stackoverflow.com/questions/2992066/code-to-strip-diacritical-marks-using-icu
auto Text::transliterate(const std::string& str) -> std::string {
	// UTF-8 std::string -> UTF-16 UnicodeString
	UnicodeString source = UnicodeString::fromUTF8(StringPiece(str));

	// Transliterate UTF-16 UnicodeString
	UErrorCode status = U_ZERO_ERROR;
	Transliterator *accentsConverter = Transliterator::createInstance("NFD; [:Mn:] Remove; NFC", UTRANS_FORWARD, status);
	accentsConverter->transliterate(source);
	if (status != U_ZERO_ERROR) {
		std::cerr << "transliteration failed: " << u_errorName(status) << " in [" << str << "]\n";
		return str;
	}
	// UTF-16 UnicodeString -> UTF-8 std::string
	std::string result;
	source.toUTF8String(result);
	return result;
}

auto Text::splitWords(std::string text) -> std::vector<std::string> {
	text = transliterate(text);
	boost::to_lower(text);
	boost::regex splitWords("[^_'/\\\\\\w]+");
	std::vector<std::string> words;
	boost::algorithm::split_regex(words, text, splitWords);

	if (words.begin() != words.end()) {
		// depending on the trim-ed-ness of the original string there could be empty elements at the begin or the end. since we match as much non-word junk as possible between there shouldn't be any in the middle. trim off potential blanks here
		if (*words.begin() == "") {
			words.erase(words.begin());
		}
		if (words.rbegin() != words.rend() && *words.rbegin() == "") {
			words.erase((words.rbegin()+1).base()); // idk wtf tbh
		}
	}
	static const boost::regex delimitingJunk("(^[-\"_'\\\\]+|[-\"_'\\\\]+$)");
	BOOST_FOREACH(auto& word, words) {
		word = boost::regex_replace(word, delimitingJunk, "", boost::match_default|boost::format_all);
	}
	return words;
}

auto Text::getSummary(const std::string& text, bool removing, int phraseLen, double lenFactor, double proximityFactor, double leadingFactor) -> WeightedPhraseVec {
	return getSummary(splitWords(text), removing, phraseLen, lenFactor, proximityFactor, leadingFactor);
}

auto weightedPhraseSort(Text::WeightedPhrase a, Text::WeightedPhrase b) -> bool {
	if (a.second == b.second) {
		return a.first.size() > b.first.size();
	}
	return a.second > b.second;
}

auto Text::getSummary(const std::vector<std::string>& words, bool removing, int phraseLen, double lenFactor, double proximityFactor, double leadingFactor) -> WeightedPhraseVec {
	// set up defaults @TODO some boost type for maybe values?
	if (phraseLen < 0) { 
		phraseLen = 3.0; 
	}
	if (lenFactor < 0) {
		lenFactor = 0.001;
	}
	if (proximityFactor < 0) {
		proximityFactor = 100.0;
	}
	if (leadingFactor < 0) {
		leadingFactor = 10.0;
	}

	double wordCount = words.size(), idx = 0.0;
	std::map<std::vector<std::string>, size_t> last;
	std::map<std::vector<std::string>, double> freq;
	for (auto it = words.begin(), end = words.end(); it != end; ++it, ++idx) {
		std::string word = *it;
		if (word == "" || word == "-" || isStopWord(word)) {
			continue;
		}
		if (removing) {
			tree->remove(word);
		}
		else {
			tree->add(word);
		}
		auto stemWord = stem(word);
		std::vector<std::pair<std::vector<std::string>, unsigned>> chunks;
		std::vector<std::string> stemPhrase;
		stemPhrase.push_back(stemWord);
		chunks.push_back(std::make_pair(stemPhrase, 1u)); 
		auto phraseWords = 1;
		for (auto off = 1u, skippedStopWords = 0u; off <= phraseLen + skippedStopWords; ++off) {
			auto nextIt = it + off;
			if (nextIt == end) {
				break;
			}
			auto nextWord = *nextIt;
			if (isStopWord(nextWord)) {
				++skippedStopWords;
				continue;
			}
			++phraseWords;
			stemPhrase.push_back(stem(nextWord));
			chunks.push_back(std::make_pair(stemPhrase, phraseWords));
		}
		double chunkPart = 0;
		for (auto chunkIt = chunks.begin(), chunkEnd = chunks.end(); chunkIt != chunkEnd; ++chunkIt, ++chunkPart) {
			auto phrase = chunkIt->first;
			phraseWords = chunkIt->second;
			if (freq.find(phrase) == freq.end()) {
				freq[phrase] = -((leadingFactor * (1.0 - (double)idx/(double)wordCount)) +
					phraseWords * lenFactor)
				;
			}
			else {
				freq[phrase] = abs(freq[phrase]);
				freq[phrase] += pow(phraseWords*10.0, 2) * lenFactor + 
					(proximityFactor * ((double)idx + 1.0 - (double)last[phrase])/(double)wordCount * ((double)chunkPart + 1.0)) + 
					(leadingFactor * (1.0 - (double)idx/(double)wordCount));
			}
			last[phrase] = idx;
		}
	}
	WeightedPhraseVec sorted;
	BOOST_FOREACH(const auto& item, freq) {
		sorted.push_back(item);
	}
	std::sort(sorted.begin(), sorted.end(), weightedPhraseSort);
	WeightedPhraseVec rv; 
	if (sorted.size()) {
		if (sorted[0].second) {
			BOOST_FOREACH(auto& phrase, sorted) {
				if (phrase.first.size() == 1 || phrase.second > 0) {
					phrase.second = abs(phrase.second);
#ifdef HG_TMP_DEBUG
					BOOST_FOREACH(const auto& word, phrase.first) {
						std::cout << word << " ";
					}
					std::cout << "- " << phrase.second << "\n";
#endif
					rv.push_back(phrase);
				}
			}
//			std::cerr << "min: " << min << ", thresh: " << (min + ((1 - min)/100)) << "\n";
//			std::copy_if(sorted.begin(), sorted.end(), std::insert_iterator<WeightedPhraseVec>(rv, rv.begin()), [min](const WeightedPhrase& phrase) {
//				return phrase.second - min > (min + ((1 - min)/5)) || phrase.first.size() == 1;
//			});
		}
	}
	return rv;
}

auto Text::getLowercase(std::string text) -> std::string {
	boost::to_lower(text);
	return text;
}

auto Text::save(std::ofstream& out) -> void {
	tree->write(out);
}
auto Text::load(std::ifstream& in) -> void {
	delete tree;
	tree = new AsciiTextNode;
	tree->read(in);
}
auto Text::getWordCount(const std::string& word) -> size_t {
	const AsciiTextNode* node = tree->traverse(word);
	if (node == nullptr) {
		return 0;
	}
	return node->getWeight();
}

}
