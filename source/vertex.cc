/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#include "vertex.h"
#include "api.h"
#include "database.cc"

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>

#include <cmath> // pow()
#include <map>

namespace HubGraph {

const v8::Persistent<v8::String> Vertex::bodyV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("body"));
const v8::Persistent<v8::String> Vertex::titleV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("title"));
const boost::regex Vertex::irrelevantChars("(<.*?>|&[a-z]{2,5};|[^-_'a-z0-9\\s]|'{2,})");
const boost::regex Vertex::whiteSpace("[\\s']+");

Vertex::Vertex(v8::Handle<v8::Object> props) {
	setProperties(props);
}

Vertex::Vertex(const std::string& props) {
	setProperties(props);
}

auto Vertex::aerate() -> void {
	setProperties(properties);
}

auto Vertex::remove(HgDatabase* db) -> void {
	v8::Handle<v8::Object> props = Api::toJsObject(properties);
	if (domain != "text" && db != nullptr) {
		if (props->Has(titleV8Sym)) {
			unparseText(db, Api::toNativeString(props->Get(titleV8Sym)));
		}
		if (props->Has(bodyV8Sym)) {
			unparseText(db, Api::toNativeString(props->Get(bodyV8Sym)));
		}
	}
}

auto Vertex::setProperties(const std::string& props) -> void {
	properties = props;
	setProperties(Api::toJsObject(props));
}

auto Vertex::setProperties(v8::Handle<v8::Object> props) -> void {
	if (properties == "") {
		properties = Api::toJson(props);
	}
	static const v8::Persistent<v8::String> idV8Sym     = v8::Persistent<v8::String>::New(v8::String::NewSymbol("id"));
	static const v8::Persistent<v8::String> domainV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("domain"));
	
	if (!props->Has(idV8Sym) || !props->Has(domainV8Sym)) {
		throw std::runtime_error("expected vertex properties to include a domain and an id");
	}
	domain = Api::toNativeString(props->Get(domainV8Sym));
	id = Api::toNativeString(props->Get(idV8Sym));
	qualifiedId = domain + ":" + id;
}

auto Vertex::internText(HgDatabase* db) -> void {
	v8::Handle<v8::Object> props = Api::toJsObject(properties);
	if (domain != "text" && db != nullptr) {
		if (props->Has(titleV8Sym)) {
			parseText(db, Api::toNativeString(props->Get(titleV8Sym)), true);
		}
		if (props->Has(bodyV8Sym)) {
			parseText(db, Api::toNativeString(props->Get(bodyV8Sym)), false);
		}
	}
	properties = Api::toJson(props);
}

auto Vertex::canonicalizeWords(std::string text) -> std::vector<std::string> {
	boost::to_lower(text);
	text = boost::regex_replace(text, irrelevantChars, " ");
	std::vector<std::string> wordList;
	boost::algorithm::split_regex(wordList, text, whiteSpace);
	return wordList;
}

auto Vertex::parseText(HgDatabase* db, const std::string& text, bool inTitle) -> void {
	BOOST_FOREACH(const auto& weightedPhrase, Text::getSummary(text)) {
		auto fullPhrase = boost::join(weightedPhrase.first, " ");
		auto weight = weightedPhrase.second;
		Vertex phraseV = Vertex("{\"domain\": \"text\", \"id\": \"" + fullPhrase + "\"}");
		db->insert(phraseV);
		db->connect(inTitle ? "title-word" : "body-word", phraseV.qualifiedId, qualifiedId, weight);
		BOOST_FOREACH(const auto& word, weightedPhrase.first) {
			Vertex wordV = Vertex("{\"domain\": \"aliases\", \"id\": \"" + word + "\"}");
			db->insert(wordV);
			db->connect("alias", wordV.qualifiedId, "text:" + fullPhrase, 1.0, false);
		}
	}
	/*
	boost::optional<Vertex> prior;
	size_t count = 0;
	BOOST_FOREACH(const auto& word, canonicalizeWords(text)) {
		std::string stem = Text::stem(word);
		if (Text::isStopWord(stem)) {
			continue;
		}
		++count;
		Text::tree->add(word);
		Vertex wordV = Vertex("{\"domain\": \"text\", \"id\": \"" + word + "\"}"),
		       stemV = Vertex("{\"domain\": \"text\", \"id\": \"" + stem + "\"}");

		db->insert(wordV);
		if (word == stem) {
			db->connect("stem", wordV.qualifiedId, wordV.qualifiedId);
		}
		else {
			db->insert(stemV);
			db->connect("stem", stemV.qualifiedId, wordV.qualifiedId);
		}
		db->connect(inTitle ? "title-word" : "body-word", stemV.qualifiedId, qualifiedId);
		if (prior) {
			db->connect("sequence", prior->qualifiedId, stemV.qualifiedId);
		}
		prior.reset(stemV);
	}
	return count;
	*/
}

auto Vertex::unparseText(HgDatabase* db, const std::string& text) -> void {
	Text::getSummary(text, true);
	/*
	boost::optional<Vertex> prior;
	BOOST_FOREACH(const auto& word, canonicalizeWords(text)) {
		std::string stem = Text::stem(word);
		if (Text::isStopWord(stem)) {
			continue;
		}
		Text::tree->remove(word);
		
		Vertex wordV = Vertex("{\"domain\": \"text\", \"id\": \"" + word + "\"}"),
		       stemV = Vertex("{\"domain\": \"text\", \"id\": \"" + stem + "\"}");

		if (prior) {
			db->disconnect("sequence", prior->qualifiedId, stemV.qualifiedId);
		}
		prior.reset(stemV);
	}
	*/
}
auto Vertex::getSpellingSuggestions(std::string base, size_t limit, size_t editDistance) -> Vertex::WeightedStrings {
	std::map<std::string, bool> tested;
	std::multimap<size_t, std::pair<size_t, std::string> > hits;

	TextNode<char>* word;
	size_t weight;
	boost::to_lower(base);
	std::vector<std::string> lastEdits = { base };
	for (size_t editCount = 0; editCount < editDistance; ++editCount) {
		std::set<std::string> newEdits;
		std::insert_iterator<std::set<std::string> > newEditInserter(newEdits, newEdits.end());
		BOOST_FOREACH(const auto& edit, lastEdits) {
			getEdits(edit, newEditInserter);	
		}
		lastEdits.clear();
		BOOST_FOREACH(const auto& edit, newEdits) {
			if (edit.length() != 0 && !tested[edit]) {
				if ((word = Text::tree->traverse(edit)) && (weight = word->getWeight())) {
					hits.insert(std::make_pair(weight * pow(2, 3*(editDistance - editCount)), std::make_pair(weight, edit)));
				}
				lastEdits.push_back(edit);
			}
			tested[edit] = true;
		}
	}
	WeightedStrings rv;
	size_t count = 0;
	BOOST_REVERSE_FOREACH(const auto& pair, hits) {
		rv.push_back(pair.second);
		if (++count == limit) {
			break;
		}
	}
	return rv;
}

auto Vertex::getEdits(const std::string& base, std::insert_iterator<std::set<std::string> >& out) -> void {
	// convert string into pairs with a break where a character is to be inserted, deleted, or replaced
	std::vector<std::pair<std::string, std::string> > splits;
	size_t len = base.length();
	for (size_t idx = 0; idx <= len; ++idx) {
		splits.push_back(std::make_pair(base.substr(0, idx), base.substr(idx, len - idx)));
	}

	// make changes at the breaks in pairs to generate all strings that are 1 edit distance away from the base
	BOOST_FOREACH(const auto& pair, splits) {
		size_t slen = pair.second.length();
		if (slen != 0) {
			// deletions
			out = pair.first + pair.second.substr(1, slen - 1);
			// transpositions
			if (slen > 1) {
				out = pair.first + pair.second.substr(1, 1) + pair.second.substr(0, 1) + pair.second.substr(2, slen - 2);
			}
			for (char ch = 'a'; ch <= 'z'; ++ch) {
				// replaces
				out = pair.first + ch + pair.second.substr(1, slen - 1);
			}
		}
		for (char ch = 'a'; ch <= 'z'; ++ch) {
			// insertions
			out = pair.first + ch + pair.second;
		}
	}
}

auto Vertex::completeTerm(const std::string& term, size_t limit, size_t threshold) -> Vertex::WeightedStrings {
	const AsciiTextNode* node = Text::tree->traverse(term);	
	if (node != nullptr) {
		return node->toStrings(limit, threshold, &Text::stem);
	}
	return WeightedStrings();
}

}


