/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_DATABASE_CC
#define HUBGRAPH_DATABASE_CC

#include "database.h"
#include <iostream>
#include <boost/graph/iteration_macros.hpp>

namespace HubGraph {

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::insert(Vertex& v, bool update) -> bool {
	auto pos = idMap.find(v.qualifiedId); 
	if (pos != idMap.end()) {
		if (update) {
			graph[pos->second].setProperties(v.getProperties());
			graph[pos->second].remove(this);
			graph[pos->second].internText(this);
			return true;
		}
		return false;
	}
	boost::optional<VertexPos> rv = boost::add_vertex(v, graph);
	if (rv) {
		idMap[v.qualifiedId] = *rv;
		if (v.getDomain() != "text") {
			graph[*rv].internText(this);
		}
		return true;
	}
	return false;
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::disconnect(const std::string& type, const std::string& fromId, const std::string& toId) -> bool {
	auto fromPos = idMap.find(fromId); 
	if (fromPos == idMap.end()) {
		return false;
	}
	auto toPos = idMap.find(toId); 
	if (toPos == idMap.end()) {
		return false;
	}
	BGL_FORALL_OUTEDGES_T(fromPos->second, e, graph, Graph) {
		if (boost::target(e, graph) == toPos->second) {
			Edge& edge = graph[e];
			if (edge.getType() == type) {
				boost::remove_edge(e, graph);
				return true;
			}
		}
	}
	return false;
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::connect(const std::string& type, const std::string& fromId, const std::string& toId, double weight, bool incrementWeight) -> bool {
	auto fromPos = idMap.find(fromId); 
	if (fromPos == idMap.end()) {
		return false;
	}
	auto toPos = idMap.find(toId); 
	if (toPos == idMap.end()) {
		return false;
	}
	BGL_FORALL_OUTEDGES_T(fromPos->second, e, graph, Graph) {
		if (boost::target(e, graph) == toPos->second) {
			Edge& edge = graph[e];
			if (edge.getType() == type) {
				if (incrementWeight) {
					edge.incrementWeight(weight);
				}
				return true;
			}
		}
	}
	std::pair<EdgePos, bool> rv = boost::add_edge(fromPos->second, toPos->second, Edge(type, weight), graph);
	return rv.second;
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::getVertex(const std::string& id) -> boost::optional<const Vertex&> {
	auto it = idMap.find(id);
	if (it == idMap.end()) {
		return boost::optional<const Vertex&>();
	}
	return boost::optional<const Vertex&>(graph[it->second]);
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::getEdgeFromVertex(const EdgePos& e) -> const Vertex& {
	return graph[boost::source(e, graph)];
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::getEdgeToVertex(const EdgePos& e) -> const Vertex& {
	return graph[boost::target(e, graph)];
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::getEdge(const EdgePos& e) -> const Edge& {
	return graph[e];
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::followIn(const std::string& fromId) -> boost::optional<std::pair<InEdgeIt, InEdgeIt> > {
	auto fromPos = idMap.find(fromId); 
	if (fromPos == idMap.end()) {
		return boost::optional<std::pair<InEdgeIt, InEdgeIt> >();
	}
	return boost::optional<std::pair<InEdgeIt, InEdgeIt> >(boost::in_edges(fromPos->second, graph));
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::followOut(const std::string& fromId) -> boost::optional<std::pair<OutEdgeIt, OutEdgeIt> > {
	auto fromPos = idMap.find(fromId); 
	if (fromPos == idMap.end()) {
		return boost::optional<std::pair<OutEdgeIt, OutEdgeIt> >();
	}
	return boost::optional<std::pair<OutEdgeIt, OutEdgeIt> >(boost::out_edges(fromPos->second, graph));
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::remove(const std::string& fromId) -> bool {
	auto pos = idMap.find(fromId); 
	if (pos == idMap.end()) {
		return false;
	}
	graph[pos->second].remove(this);
	boost::clear_vertex(pos->second, graph);
	boost::remove_vertex(pos->second, graph);

	// TODO it may be possible to avoid regenerating this if we go back to letting boost keep track of the string->vertex_descriptor mapping
	idMap.clear();
	BGL_FORALL_VERTICES_T(v, graph, Graph) {
		idMap[graph[v].qualifiedId] = v;
	}
	return true;
}

template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::aerate() -> void {
	BGL_FORALL_VERTICES_T(v, graph, Graph) {
		graph[v].aerate();
		idMap[graph[v].qualifiedId] = v;
	}
}
	
template <typename Vertex, typename Edge>
auto Database<Vertex, Edge>::echoProperties() -> void {
	BGL_FORALL_VERTICES_T(v, graph, Graph) {
		std::cout << graph[v].getProperties() << std::endl;
	}
}

}

#endif
