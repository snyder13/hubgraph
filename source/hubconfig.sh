#!/bin/bash
#   Copyright 2013 HUBzero Foundation, LLC
#   
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#   
#       http://www.apache.org/licenses/LICENSE-2.0
#   
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

DEFAULT_GIT=git@bitbucket.org:snyder13/hubgraph-hubzero-views.git

uid="$(id -u)"

function out {
	echo $@ | fold -s
}

function asRoot {
	if [ "$uid" == 0 ]; then
		$@
	else
		sudo $@
	fi;
}

out 'This script is intended to help get hubgraph working as the search provider for the HUBzero CMS'
out

if [ -e client/hubzero ]; then
	out 'Updating client/hubzero'
	git submodule update
else
	out 'Initializing client/hubzero'
	read -p "Git repo to use for the HUBzero client implementation? [default: ${DEFAULT_GIT}]: " git 
	if [ "$git" == "" ]; then
		git=$DEFAULT_GIT
	fi;
	git submodule add -f "$git" client/hubzero
fi;

if [ -z $(grep '^hubgraph:' /etc/passwd) ]; then
	out 'Setting up hubgraph user'
	adduser --disabled-login --no-create-home --home /var/hubgraph --quiet --shell /bin/false hubgraph
fi;

if [ ! -e /etc/hubgraph.conf ]; then
	out 'Installing configuration /etc/hubgraph.conf'
	asRoot cp client/hubzero/hubgraph.conf /etc/
fi;

if [ ! -e /etc/init.d/hubgraph ]; then
	out 'Installing initscript /etc/init.d/hubgraph'
	asRoot cp client/hubzero/hubgraph-init /etc/init.d/hubgraph
fi;

if [ ! -e client/hubzero/resources ]; then
	out 'Selecting default hub resources for data source'
	cd client/hubzero && ln -s hub-standard-resources resources && cd ../..
fi;

read -p "Add hubgraph at default runlevels so that it starts automatically? [y/N]" rl
if [ "$rl" == "y" ] || [ "$rl" == "Y" ]; then
	asRoot update-rc.d hubgraph defaults
fi;

out "Verifying node modules"
stage/bin/npm install iniparser mysql daemon

out
out
out 'Almost there!'
out '-------------'
out 'Edit /etc/hubgraph.conf to add credentials allowing Hubgraph read access to the jos_* tables in your hub MySQL database, and full access to the hg_update_queue table.'
out
out "If you don't have an hg_update_queue table, apply the file /var/hubgraph/client/hubzero/triggers.sql to your database using superuser credentials."
out
out '"/etc/init.d/hubgraph start" will start the server and index your hub.'
out
out 'After this has happened, visit "http://localhost/hubgraph?task=settings" (replacing the hostname as appropriate) to make sure that the CMS software is able to communicate with the hub. All you need to do here is verify the port matches the socket path or port number you specified in hubgraph.conf.'
out
out 'If you run into any problems or have any suggestions kindly (or otherwise) email the author: Steven Snyder <snyder13@purdue.edu>'

