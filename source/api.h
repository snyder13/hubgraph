/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_API_H
#define HUBGRAPH_API_H

#include <string>

#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <v8.h>
#include <node.h>
#pragma GCC diagnostic warning "-Wunused-parameter"

#include "database.h" /// Database is a template class, I need its implementation to instantiate the appropriate template, and I don't like putting it all in the header file

namespace HubGraph {

class Vertex;
class Edge;
class ResultComparator;
template <typename Vertex, typename Edge> class Database;
typedef Database<Vertex, Edge> HgDatabase;

class Api
{
	typedef std::pair<double, std::pair<std::string, std::string> > Result;
	class ResultComparator
	{
	public:
		// first part of Result structure is the weight, sort by that, descending
		auto operator()(const Api::Result& a, const Api::Result&b) -> bool {
			return a.first < b.first;
		}
	};
	typedef std::priority_queue<Result, std::vector<Result>, ResultComparator> Results;
public:
	static auto init(v8::Handle<v8::Object> target) -> void;

	// root jsapi
	// add a new vertex from the given object, which should at a minimum define keys "domain" and "id"
	static auto             add(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// remove the vertex with the given domain, id pair
	static auto          remove(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// guess what terms one might be looking for from a prefix, given the commonality of phrases in the databse 
	static auto    completeTerm(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// add an edge between two vertices
	static auto         connect(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// delete an edge between two vertices
	static auto      disconnect(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// utility to create an empty search result object, which is helpful because stdlib.js augments it
	static auto           empty(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// determine how often a word occurs in the database -- @DEPRECATED with move to phrase-based indexing
	static auto    getWordCount(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// reload database state from boost serialization
	static auto            load(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// serialize the database state to disc
	static auto            save(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// start a query by selected a base vertex
	static auto          select(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// convert a word to its stem
	static auto            stem(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// suggest a spelling for word given the frequency of words like it in the databse
	static auto suggestSpelling(const v8::Arguments& args) -> v8::Handle<v8::Value>; 
	// determine whether the given word is considered to be a stop word, ie, a word without meaningful semantics like "the", "as", "etc", etc
	static auto      isStopWord(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	static auto    getStopWords(const v8::Arguments&)      -> v8::Handle<v8::Value>;
	// get the list of stop words compiled id
	static auto     stopWordMap(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	// return a list of words and phrases, with weights, describing the supposed content of the text
	static auto      getSummary(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	static auto      splitStems(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	static auto          unstem(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	static auto   transliterate(const v8::Arguments& args) -> v8::Handle<v8::Value>;

	static auto in(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	static auto out(const v8::Arguments& args) -> v8::Handle<v8::Value>;
	static auto use(const v8::Arguments& args) -> v8::Handle<v8::Value>;

	static auto test(const v8::Arguments& args) -> v8::Handle<v8::Value>;

	static auto toNativeString(v8::Handle<v8::Value> js) -> std::string;
	static auto toNativeString(v8::Handle<v8::String> js) -> std::string;
	static auto toJsObject(const std::string& str) -> v8::Handle<v8::Object>;
	static auto toJson(const v8::Handle<v8::Object> obj) -> std::string;
private:
	template <typename T>
	static auto addEdge(std::map<std::pair<std::string, std::string>, double>& map, const T it, const std::string& type, double baseWeight) -> void;
	static auto follow(const v8::Arguments& args, bool in) -> v8::Handle<v8::Value>;
	static auto selector(const v8::Arguments& args) -> v8::Handle<v8::Value> {
		v8::HandleScope scope;
		return scope.Close(args.This());
	}
	static Results newResults() {
		return std::priority_queue<Result, std::vector<Result>, ResultComparator>();
	}
	static auto attachCallbacks(Results& res, bool followAliases = true) -> v8::Handle<v8::Object>;

	static v8::Persistent<v8::Object> target;
	static HgDatabase* db;
};



}

#endif
