/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_DATABASE_H
#define HUBGRAPH_DATABASE_H

#include <unordered_map>
#include <list>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/adj_list_serialize.hpp>
#include <boost/tuple/tuple.hpp>

namespace HubGraph {

template<typename Vertex, typename Edge>
class Database : public boost::noncopyable
{
	typedef boost::adjacency_list<boost::listS, boost::listS, boost::bidirectionalS, Vertex, Edge> Graph;
	typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexPos;
	typedef typename boost::graph_traits<Graph>::edge_descriptor EdgePos;

	template<typename Archive, typename VertexT, typename EdgeT>
	friend auto boost::serialization::serialize(Archive&, Database<VertexT, EdgeT>&, const unsigned int) -> void;
public:
	typedef typename boost::graph_traits<Graph>::in_edge_iterator InEdgeIt;
	typedef typename boost::graph_traits<Graph>::out_edge_iterator OutEdgeIt;

	auto insert(Vertex& v, bool update = true) -> bool;
	auto remove(const std::string& id) -> bool;
	auto connect(const std::string& type, const std::string& fromId, const std::string& toId, double weight = 1.0, bool incrementWeight = true) -> bool;
	auto disconnect(const std::string& type, const std::string& fromId, const std::string& toId) -> bool;
	auto followIn(const std::string& toId) -> boost::optional<std::pair<InEdgeIt, InEdgeIt> >;
	auto followOut(const std::string& fromId) -> boost::optional<std::pair<OutEdgeIt, OutEdgeIt> >;
	auto aerate() -> void;
	auto getVertex(const std::string& id) -> boost::optional<const Vertex&>;

	auto getEdgeFromVertex(const EdgePos& e) -> const Vertex&;
	auto getEdgeToVertex(const EdgePos& e) -> const Vertex&;
	auto getEdge(const EdgePos& e) -> const Edge&;

	auto echoProperties() -> void; /// @TESTING
private:	
	Graph graph;
	std::unordered_map<std::string, VertexPos> idMap;
};

}

namespace boost { namespace serialization {

template<typename Archive, typename Vertex, typename Edge>
auto serialize(Archive& ar, HubGraph::Database<Vertex, Edge>& db, const unsigned int) -> void {
	ar & db.graph;
}

} }

#endif
