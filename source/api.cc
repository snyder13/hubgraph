/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#include "vertex.h"
#include "api.h"
#include "database.cc" /// Database is a template class, I need its implementation to instantiate the appropriate template, and I don't like putting it all in the header file
#include "edge.h"
#include "text.h"

#include <iostream>
#include <fstream>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/utility.hpp>
#include <boost/foreach.hpp>

namespace HubGraph {

#define REQ_OBJ_ARG(I, VAR)                                      \
	if (args.Length() < (I) || !args[I - 1]->IsObject())          \
		return v8::ThrowException(v8::Exception::TypeError(        \
			v8::String::New("Argument " #I " must be an object"))); \
	v8::Local<v8::Object> VAR = v8::Object::Cast(*args[I - 1]);

#define REQ_STR_ARG(I, VAR)                                      \
	if (args.Length() < (I) || !args[I - 1]->IsString())          \
		return v8::ThrowException(v8::Exception::TypeError(        \
			v8::String::New("Argument " #I " must be a string")));  \
	std::string VAR = toNativeString(args[I - 1]->ToString());

#define REQ_NUM_ARG(I, VAR)                                     \
	if (args.Length() < (I) || !args[I - 1]->IsNumber())         \
		return v8::ThrowException(v8::Exception::TypeError(       \
			v8::String::New("Argument " #I " must be a number"))); \
	double VAR = args[I - 1]->NumberValue();

#define STRINGIFY_ARG(I, VAR)                                    \
	if (args.Length() < (I))                                      \
		return v8::ThrowException(v8::Exception::TypeError(        \
			v8::String::New("Argument " #I " must be a string")));  \
	std::string VAR = toNativeString(args[I - 1]->ToString());


v8::Persistent<v8::Object> Api::target;
HgDatabase* Api::db = new HgDatabase;

auto Api::init(v8::Handle<v8::Object> target) -> void {
	target->Set(v8::String::NewSymbol("add"),             v8::FunctionTemplate::New(add)->GetFunction());
	target->Set(v8::String::NewSymbol("completeTerm"),    v8::FunctionTemplate::New(completeTerm)->GetFunction());
	target->Set(v8::String::NewSymbol("connect"),         v8::FunctionTemplate::New(connect)->GetFunction());
	target->Set(v8::String::NewSymbol("disconnect"),      v8::FunctionTemplate::New(disconnect)->GetFunction());
	target->Set(v8::String::NewSymbol("empty"),           v8::FunctionTemplate::New(empty)->GetFunction());
	target->Set(v8::String::NewSymbol("getWordCount"),    v8::FunctionTemplate::New(getWordCount)->GetFunction());
	target->Set(v8::String::NewSymbol("load"),            v8::FunctionTemplate::New(load)->GetFunction());
	target->Set(v8::String::NewSymbol("remove"),          v8::FunctionTemplate::New(remove)->GetFunction());
	target->Set(v8::String::NewSymbol("save"),            v8::FunctionTemplate::New(save)->GetFunction());
	target->Set(v8::String::NewSymbol("select"),          v8::FunctionTemplate::New(select)->GetFunction());
	target->Set(v8::String::NewSymbol("stem"),            v8::FunctionTemplate::New(stem)->GetFunction());
	target->Set(v8::String::NewSymbol("unstem"),          v8::FunctionTemplate::New(unstem)->GetFunction());
	target->Set(v8::String::NewSymbol("suggestSpelling"), v8::FunctionTemplate::New(suggestSpelling)->GetFunction());
	target->Set(v8::String::NewSymbol("stopWordMap"),     v8::FunctionTemplate::New(stopWordMap)->GetFunction());
	target->Set(v8::String::NewSymbol("isStopWord"),      v8::FunctionTemplate::New(isStopWord)->GetFunction());
	target->Set(v8::String::NewSymbol("getStopWords"),    v8::FunctionTemplate::New(getStopWords)->GetFunction());
	target->Set(v8::String::NewSymbol("splitStems"),      v8::FunctionTemplate::New(splitStems)->GetFunction());
	target->Set(v8::String::NewSymbol("test"),            v8::FunctionTemplate::New(test)->GetFunction());
	target->Set(v8::String::NewSymbol("getSummary"),      v8::FunctionTemplate::New(getSummary)->GetFunction());
	target->Set(v8::String::NewSymbol("transliterate"),   v8::FunctionTemplate::New(transliterate)->GetFunction());

	target->Set(v8::String::NewSymbol("Selector"), v8::FunctionTemplate::New(selector)->GetFunction());
	Api::target = v8::Persistent<v8::Object>::New(target);
}

auto Api::transliterate(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, str);
	return scope.Close(v8::String::New(Text::transliterate(str).c_str()));
}

auto Api::splitStems(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, str);
	v8::Handle<v8::Array> rv = v8::Array::New();
	unsigned idx = 0;
	BOOST_FOREACH(const auto& word, Text::splitWords(str)) {
		if (!Text::isStopWord(word)) {
			rv->Set(idx++, v8::String::New(Text::stem(word).c_str()));
		}
	}

	return scope.Close(rv);
}

auto Api::getSummary(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	static v8::Persistent<v8::String> phraseV8Sym  = v8::Persistent<v8::String>::New(v8::String::NewSymbol("phrase"));
	static v8::Persistent<v8::String> weightV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("weight"));
	v8::HandleScope scope;
	REQ_STR_ARG(1, text);
	text = Text::getLowercase(text);
	Text::WeightedPhraseVec summary = Text::getSummary(text);
	v8::Handle<v8::Array> rv = v8::Array::New();
	unsigned idx = 0;
	BOOST_FOREACH(const auto& phrase, summary) {
		v8::Handle<v8::Object> item = v8::Object::New();
		item->Set(phraseV8Sym, v8::String::New(Text::unstem(phrase.first).c_str()));
		item->Set(weightV8Sym, v8::Number::New(phrase.second));
		rv->Set(idx++, item);
	}
	return scope.Close(rv);
}

auto Api::empty(const v8::Arguments&) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	Results rv = newResults();
	return scope.Close(attachCallbacks(rv));
}

auto Api::isStopWord(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, word);
	return scope.Close(v8::Boolean::New(Text::isStopWord(word) || Text::isStopWord(Text::stem(word))));
}

auto Api::getStopWords(const v8::Arguments&) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	v8::Handle<v8::Array> rv = v8::Array::New();
	size_t idx = 0;
	BOOST_FOREACH(const auto& term, Text::getStopWordMap()) {
		rv->Set(idx++, v8::String::New(term.first.c_str()));
	}
	return scope.Close(rv);
}

auto Api::stopWordMap(const v8::Arguments&) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	v8::Handle<v8::Object> map = v8::Object::New();
	BOOST_FOREACH(const auto& word, Text::getStopWordMap()) {
		map->Set(v8::String::New(word.first.c_str()), v8::Boolean::New(true));
	}
	return scope.Close(map);
}

auto Api::getWordCount(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, word);
	return scope.Close(v8::Number::New(Text::getWordCount(word)));
}

auto Api::stem(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, word);
	return scope.Close(v8::String::New(Text::stem(word).c_str()));
}

auto Api::unstem(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, word);
	return scope.Close(v8::String::New(Text::unstem(word).c_str()));
}

auto Api::add(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_OBJ_ARG(1, props);
		
	try {
		Vertex v(props);
		return scope.Close(v8::Boolean::New(db->insert(v)));
	}
	catch (std::runtime_error& ex) {
		return v8::ThrowException(v8::Exception::Error(v8::String::New(ex.what())));
	}
}

auto Api::remove(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, domain);
	STRINGIFY_ARG(2, id);
	return scope.Close(v8::Boolean::New(db->remove(domain + ":" + id)));
}

auto Api::disconnect(const v8::Arguments& args) -> v8::Handle<v8::Value> {	
	v8::HandleScope scope;
	REQ_STR_ARG(1, type);
	REQ_STR_ARG(2, fromDomain);
	STRINGIFY_ARG(3, fromId);
	REQ_STR_ARG(4, toDomain);
	STRINGIFY_ARG(5, toId);

	return scope.Close(v8::Boolean::New(db->disconnect(type, fromDomain + ":" + fromId, toDomain + ":" + toId)));
}

auto Api::connect(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, type);
	REQ_STR_ARG(2, fromDomain);
	STRINGIFY_ARG(3, fromId);
	REQ_STR_ARG(4, toDomain);
	STRINGIFY_ARG(5, toId);

	double weight = 1.0;
	if (args.Length() == 6) {
		REQ_NUM_ARG(6, wt);
		weight = wt;
	}

	return scope.Close(v8::Boolean::New(db->connect(type, fromDomain + ":" + fromId, toDomain + ":" + toId, weight)));
}

auto Api::in(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	return follow(args, true); 
}

auto Api::out(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	return follow(args, false); 
}

auto Api::follow(const v8::Arguments& args, bool in) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;

	REQ_STR_ARG(1, type);
	std::string domain = "";
	if (args.Length() == 2) {
		domain = toNativeString(args[1]->ToString());
	}
	bool followAliases = domain != "aliases";

	static v8::Persistent<v8::String> itemsV8Sym  = v8::Persistent<v8::String>::New(v8::String::NewSymbol("items"));
	static v8::Persistent<v8::String> domainV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("domain"));
	static v8::Persistent<v8::String> idV8Sym     = v8::Persistent<v8::String>::New(v8::String::NewSymbol("id"));
	static v8::Persistent<v8::String> weightV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("weight"));
	
	Results rv = newResults();
	v8::Handle<v8::Array> items = v8::Handle<v8::Array>::Cast(args.This()->Get(itemsV8Sym));
	std::map<std::pair<std::string, std::string>, double> map;

	std::vector<std::pair<std::string, double> > aliases;
	size_t count = items->Length();
	for (size_t idx = 0; idx < count; ++idx) {
		v8::Handle<v8::Object> item = v8::Handle<v8::Object>::Cast(items->Get(idx));
		if (in) {
			auto vertices = db->followIn(toNativeString(item->Get(domainV8Sym)->ToString()) + ":" + toNativeString(item->Get(idV8Sym)->ToString()));
			if (vertices) {
				double baseWeight = item->Get(weightV8Sym)->NumberValue();
				for (auto it = vertices->first; it != vertices->second; ++it) {
					auto edge = db->getEdge(*it);
					if (edge.getType() == type) {
						const Vertex& v = db->getEdgeFromVertex(*it);
						if (followAliases && v.getDomain() == "aliases") {
							aliases.push_back(std::make_pair(v.getId(), baseWeight));
						}
						else if (domain == "" || v.getDomain() == domain) {
							auto qualifiedId = std::make_pair(v.getDomain(), v.getId());
							if (map[qualifiedId]) {
								map[qualifiedId] += baseWeight * edge.getWeight();
							}
							else {
								map[qualifiedId] = baseWeight * edge.getWeight();
							}
						}
					}
				}
			}
		}
		else {
			auto vertices = db->followOut(toNativeString(item->Get(domainV8Sym)->ToString()) + ":" + toNativeString(item->Get(idV8Sym)->ToString()));
			if (vertices) {
				double baseWeight = item->Get(weightV8Sym)->NumberValue();
				for (auto it = vertices->first; it != vertices->second; ++it) {
					auto edge = db->getEdge(*it);
					if (edge.getType() == type) {
						const Vertex& v = db->getEdgeToVertex(*it);
						if (followAliases && v.getDomain() == "aliases") {
							aliases.push_back(std::make_pair(v.getId(), baseWeight));
						}
						else if (domain == "" || v.getDomain() == domain) {
							auto qualifiedId = std::make_pair(v.getDomain(), v.getId());
							if (map[qualifiedId]) {
								map[qualifiedId] += baseWeight * edge.getWeight();
							}
							else {
								map[qualifiedId] = baseWeight * edge.getWeight();
							}
						}
					}
				}
			}
		}
	}

/*

		if (followAliases && item.second.first == "aliases") {
			auto vertices = db->followOut("aliases:" + item.second.second);
			if (vertices) {
				double baseWeight = item.first;
				for (auto it = vertices->first; it != vertices->second; ++it) {
					auto edge = db->getEdge(*it);
					if (edge.getType() == "alias") {
						const Vertex& v = db->getEdgeToVertex(*it);

						v8::Handle<v8::Object> jsItem = v8::Object::New();
						jsItem->Set(domainV8Sym, v8::String::New(v.getDomain().c_str()));
						jsItem->Set(idV8Sym,     v8::String::New(v.getId().c_str()));
						jsItem->Set(weightV8Sym, v8::Number::New(edge.getWeight() * baseWeight)); // * ?
						items->Set(idx++, jsItem);
					}
				}
			}
		}

*/
	if (followAliases) {
		BOOST_FOREACH(const auto& al, aliases) {
			auto vertices = db->followOut("aliases:" + al.first);
			double baseWeight = al.second;
			for (auto it = vertices->first; it != vertices->second; ++it) {
				auto edge = db->getEdge(*it);
				if (edge.getType() == "alias") {
					const Vertex& v = db->getEdgeToVertex(*it);
					auto qualifiedId = std::make_pair(v.getDomain(), v.getId());
					if (map[qualifiedId]) {
						map[qualifiedId] += baseWeight * edge.getWeight();
					}
					else {
						map[qualifiedId] = baseWeight * edge.getWeight();
					}
				}
			}
		}
	}
	BOOST_FOREACH(auto& v, map) {
		rv.push(std::make_pair(v.second, v.first));
	}

	return scope.Close(attachCallbacks(rv, domain != "aliases"));
}

auto Api::use(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;

	static v8::Persistent<v8::String> itemsV8Sym  = v8::Persistent<v8::String>::New(v8::String::NewSymbol("items"));
	static v8::Persistent<v8::String> domainV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("domain"));
	static v8::Persistent<v8::String> idV8Sym     = v8::Persistent<v8::String>::New(v8::String::NewSymbol("id"));
	static v8::Persistent<v8::String> weightV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("weight"));
	v8::Handle<v8::Array> items = v8::Handle<v8::Array>::Cast(args.This()->Get(itemsV8Sym));

	bool keys = false;
	std::map<std::string, v8::Handle<v8::String> > want;
	if (args.Length() >= 1 && args[0]->IsArray()) {
		keys = true;
		v8::Handle<v8::Array> ar = v8::Handle<v8::Array>::Cast(args[0]);
		for (size_t idx = 0; idx < ar->Length(); ++idx) {
			auto str = toNativeString(ar->Get(idx));
			want[str] = v8::String::NewSymbol(str.c_str());
		}
	}
	size_t count = items->Length();
	for (size_t idx = 0; idx < count; ++idx) {
		v8::Handle<v8::Object> item = v8::Handle<v8::Object>::Cast(items->Get(idx));
		boost::optional<const Vertex&> v = db->getVertex(
			toNativeString(item->Get(domainV8Sym)->ToString()) + ":" + toNativeString(item->Get(idV8Sym)->ToString())
		);
		if (v) {
			v8::Handle<v8::Object> json = toJsObject(v->getProperties());
			if (keys) {
				auto props = v8::Object::New();
				props->Set(domainV8Sym, item->Get(domainV8Sym));
				props->Set(idV8Sym,     item->Get(idV8Sym));
				props->Set(weightV8Sym, item->Get(weightV8Sym));
				BOOST_FOREACH(const auto& entry, want) {
					if (json->Has(entry.second)) {
						props->Set(entry.second, json->Get(entry.second));
					}	
				}
				items->Set(idx, props);		
			}
			else {
				json->Set(weightV8Sym, item->Get(weightV8Sym));
				items->Set(idx, json);
			}
		}
	}	
	return scope.Close(args.This());
}

auto Api::select(const v8::Arguments& args) -> v8::Handle<v8::Value> {	
	v8::HandleScope scope;
	auto rv = newResults();
	REQ_STR_ARG(1, domain);
	STRINGIFY_ARG(2, id);
	rv.push(std::make_pair(1.0, std::make_pair(domain, id)));
	return scope.Close(attachCallbacks(rv, domain != "aliases"));
}

auto Api::suggestSpelling(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;

	static v8::Persistent<v8::String> wordV8Sym   = v8::Persistent<v8::String>::New(v8::String::NewSymbol("word"));
	static v8::Persistent<v8::String> weightV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("weight"));
	
	REQ_STR_ARG(1, word);
	
	v8::Handle<v8::Array> rv = v8::Array::New();
	size_t idx = 0;
	BOOST_FOREACH(const auto& suggestion, Vertex::getSpellingSuggestions(word)) {
		v8::Handle<v8::Object> item = v8::Object::New();
		item->Set(wordV8Sym, v8::String::New(suggestion.second.c_str()));
		item->Set(weightV8Sym, v8::Number::New(suggestion.first));
		rv->Set(idx++, item);
	}
	return scope.Close(rv);
}
auto Api::completeTerm(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	size_t argc = args.Length();

	if (argc < 1 || argc > 3 || !args[0]->IsString() || (argc >= 2 && !args[1]->IsNumber()) || (argc >= 3 && !args[2]->IsNumber())) {
		return v8::ThrowException(v8::Exception::TypeError(v8::String::New("completeTerms expected arguments of the form (string to be completed, optional integer limit for results, optional threshold for result occurrences)")));
	}

	size_t limit = 20,
	   threshold = 0;
	if (argc >= 2) {
		limit = args[1]->NumberValue();
	}
	if (argc == 3) {
		threshold = args[2]->NumberValue();
	}
	
	v8::Handle<v8::Array> rv = v8::Array::New();
	REQ_STR_ARG(1, term);
	auto stemTerm = Text::stem(term);
	size_t idx = 0;
	std::map<std::string, bool> stems;
	BOOST_FOREACH(const auto& weightedString, Vertex::completeTerm(stemTerm, limit*10, threshold)) {
		std::string stem = Text::stem(stemTerm + weightedString.second); 
		if (stems.find(stem) == stems.end()) {
			stems[stem] = true;
			v8::Handle<v8::Array> item = v8::Array::New();
			rv->Set(idx++, item);
			item->Set(0, v8::String::New(Text::unstem(stem).c_str()));
			item->Set(1, v8::Number::New(weightedString.first));
			if (idx == limit) {
				break;
			}
		}
	}
	return scope.Close(rv); 
}

auto Api::load(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, path);
	boost::checked_delete(db);
	db = new HgDatabase;

	try {
		std::ifstream dat(path + "/db.dat");
		boost::archive::binary_iarchive ia(dat);
		ia >> *db;

		db->aerate();

		std::ifstream text(path + "/text.dat");
		Text::load(text);
	}
	catch (boost::archive::archive_exception& ex) {
		return scope.Close(v8::Boolean::New(false));
	}

	return scope.Close(v8::Boolean::New(true));
}	

auto Api::save(const v8::Arguments& args) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	REQ_STR_ARG(1, path);

	std::ofstream dat(path + "/db.dat");
	boost::archive::binary_oarchive oa(dat);
	oa << *db;

	std::ofstream text(path + "/text.dat");
	Text::save(text);

	return scope.Close(v8::Boolean::New(true));
}	

auto Api::test(const v8::Arguments&) -> v8::Handle<v8::Value> {
	v8::HandleScope scope;
	db->echoProperties();
	return scope.Close(v8::Boolean::New(true));
}

auto Api::toNativeString(v8::Handle<v8::Value> js) -> std::string { 
	return *v8::String::Utf8Value(js->ToString()); 
}

auto Api::toNativeString(v8::Handle<v8::String> js) -> std::string { 
	return *v8::String::Utf8Value(js); 
}

auto Api::toJsObject(const std::string& str) -> v8::Handle<v8::Object> {
	static v8::Persistent<v8::Object> global = v8::Persistent<v8::Object>::New(v8::Context::GetCurrent()->Global());
	static v8::Persistent<v8::Function> parse = v8::Persistent<v8::Function>::New(v8::Handle<v8::Function>::Cast(v8::Handle<v8::Object>::Cast(global->Get(v8::String::NewSymbol("JSON")))->Get(v8::String::NewSymbol("parse"))));

	v8::Handle<v8::Value> argv[1] = { v8::String::New(str.c_str()) }; 
	v8::Handle<v8::Value> rv = parse->Call(global, 1, argv);
	return v8::Handle<v8::Object>::Cast(rv);
}

auto Api::toJson(const v8::Handle<v8::Object> obj) -> std::string {
	static v8::Persistent<v8::Object> global = v8::Persistent<v8::Object>::New(v8::Context::GetCurrent()->Global());
	static v8::Persistent<v8::Function> stringify = v8::Persistent<v8::Function>::New(v8::Handle<v8::Function>::Cast(v8::Handle<v8::Object>::Cast(global->Get(v8::String::NewSymbol("JSON")))->Get(v8::String::NewSymbol("stringify"))));

	v8::Handle<v8::Value> argv[1] = { obj }; 
	v8::Handle<v8::Value> rv = stringify->Call(global, 1, argv);
	v8::Handle<v8::String> str = v8::Handle<v8::String>::Cast(rv);
	return toNativeString(str); 
}

auto Api::attachCallbacks(Results& res, bool followAliases) -> v8::Handle<v8::Object> {
	static v8::Persistent<v8::String> itemsV8Sym  = v8::Persistent<v8::String>::New(v8::String::NewSymbol("items"));
	static v8::Persistent<v8::String> domainV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("domain"));
	static v8::Persistent<v8::String> idV8Sym     = v8::Persistent<v8::String>::New(v8::String::NewSymbol("id"));
	static v8::Persistent<v8::String> weightV8Sym = v8::Persistent<v8::String>::New(v8::String::NewSymbol("weight"));
	static v8::Persistent<v8::String> inV8Sym     = v8::Persistent<v8::String>::New(v8::String::NewSymbol("in"));
	static v8::Persistent<v8::String> outV8Sym    = v8::Persistent<v8::String>::New(v8::String::NewSymbol("out"));
	static v8::Persistent<v8::String> useV8Sym    = v8::Persistent<v8::String>::New(v8::String::NewSymbol("use"));

	static v8::Persistent<v8::FunctionTemplate> inTpl  = v8::Persistent<v8::FunctionTemplate>::New(v8::FunctionTemplate::New(in));
	static v8::Persistent<v8::FunctionTemplate> outTpl = v8::Persistent<v8::FunctionTemplate>::New(v8::FunctionTemplate::New(out));
	static v8::Persistent<v8::FunctionTemplate> useTpl = v8::Persistent<v8::FunctionTemplate>::New(v8::FunctionTemplate::New(use));

	v8::Handle<v8::Object> rv = v8::Handle<v8::Function>::Cast(target->Get(v8::String::NewSymbol("Selector")))->NewInstance();
	v8::Handle<v8::Array> items = v8::Array::New();
	size_t idx = 0;

	while (!res.empty()) {
		const Result& item = res.top();
		if (followAliases && item.second.first == "aliases") {
			auto vertices = db->followOut("aliases:" + item.second.second);
			if (vertices) {
				double baseWeight = item.first;
				for (auto it = vertices->first; it != vertices->second; ++it) {
					auto edge = db->getEdge(*it);
					if (edge.getType() == "alias") {
						const Vertex& v = db->getEdgeToVertex(*it);

						v8::Handle<v8::Object> jsItem = v8::Object::New();
						jsItem->Set(domainV8Sym, v8::String::New(v.getDomain().c_str()));
						jsItem->Set(idV8Sym,     v8::String::New(v.getId().c_str()));
						jsItem->Set(weightV8Sym, v8::Number::New(edge.getWeight() * baseWeight)); // * ?
						items->Set(idx++, jsItem);
					}
				}
			}
		}
		else {
			v8::Handle<v8::Object> jsItem = v8::Object::New();
			jsItem->Set(domainV8Sym, v8::String::New(item.second.first.c_str()));
			jsItem->Set(idV8Sym,     v8::String::New(item.second.second.c_str()));
			jsItem->Set(weightV8Sym, v8::Number::New(item.first));
			items->Set(idx++, jsItem);
		}
		res.pop();
	}
	rv->Set(itemsV8Sym, items);
	rv->Set(inV8Sym,  inTpl->GetFunction());
	rv->Set(outV8Sym, outTpl->GetFunction());
	rv->Set(useV8Sym, useTpl->GetFunction());
	return rv;
}

NODE_MODULE(hubgraph, Api::init);

}
