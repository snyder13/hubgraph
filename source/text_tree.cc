/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
#ifndef HUBGRAPH_TEXT_TREE_CC
#define HUBGRAPH_TEXT_TREE_CC
#include "text_tree.h"

#include <map>
#include <stdexcept>

#include <boost/filesystem.hpp>
#include "text.h"

namespace HubGraph
{

std::ostream& operator<<(std::ostream& os, const TextNode<char>& node) {
	for (TextNode<char>::Tree::const_iterator ci = node.tree.begin(); ci != node.tree.end(); ++ci) {
		os << std::string((node.depth - 1) * 2, '-') << ci->first << '[' << ci->second->weight << "]: \n" << *ci->second;
	}
	return os;
}

std::wostream& operator<<(std::wostream& os, const TextNode<wchar_t>& node) {
	for (TextNode<wchar_t>::Tree::const_iterator ci = node.tree.begin(); ci != node.tree.end(); ++ci) {
		os << std::wstring((node.depth - 1) * 2, L'-') << ci->first << L'[' << ci->second->weight << L"]: \n" << *ci->second;
	}
	return os;
}

template <typename charT>
bool TextNode<charT>::WeightedStringCmp(TextNode<charT>::WeightedString a, TextNode<charT>::WeightedString b) { 
	return a.first > b.first || (a.first == b.first && a.second < b.second); 
}

template <typename charT>
std::list<std::pair<size_t, std::basic_string<charT> > > TextNode<charT>::toStrings(size_t limit, size_t threshold, Stemmer* stemmer) const {
	std::list<std::pair<size_t, std::basic_string<charT> > > strings = _toStrings();

	std::map<std::basic_string<charT>, std::pair<size_t, std::basic_string<charT> > > stems;
	for (typename std::list<std::pair<size_t, std::basic_string<charT> > >::const_iterator ci = strings.begin(); ci != strings.end(); ++ci) {
		std::string stem = (*stemmer)(ci->second);
		if (stems.find(stem) == stems.end()) {
			stems[stem] = *ci;
		}
		else {
			stems[stem].first += ci->first;
		}
	}
	strings.erase(strings.begin(), strings.end());
	for (typename std::map<std::basic_string<charT>, std::pair<size_t, std::basic_string<charT> > >::const_iterator ci = stems.begin(); ci != stems.end(); ++ci) {
		strings.push_back(ci->second);
	}

	strings.sort(TextNode<charT>::WeightedStringCmp);
	
	std::list<std::pair<size_t, std::basic_string<charT> > > rv;
	size_t idx = 0;
	for (typename std::list<std::pair<size_t, std::basic_string<charT> > >::const_iterator ci = strings.begin(); ci != strings.end(); ++ci) {
		if ((limit && ++idx > limit) || ci->first < threshold) {
			break;
		}
		rv.push_back(*ci);
	}

	return rv;
}

template <typename charT>
typename TextNode<charT>::WeightedStringList TextNode<charT>::_toStrings() const {
	TextNode<charT>::WeightedStringList rv;
	std::map<std::string, std::string> stemToFirstFullForm;
	for (typename TextNode<charT>::Tree::const_iterator ci = tree.begin(); ci != tree.end(); ++ci) {
		std::basic_string<charT> str(1, ci->first);
		if (ci->second->weight) {
			rv.push_back(TextNode<charT>::WeightedString(ci->second->weight, str));
		}
		TextNode<charT>::WeightedStringList subStrs = ci->second->_toStrings();
		
		for (typename TextNode<charT>::WeightedStringList::const_iterator ici = subStrs.begin(); ici != subStrs.end(); ++ici) {
			rv.push_back(TextNode<charT>::WeightedString(ici->first, str + ici->second));
		}
	}
	return rv;
}

//template <char>
template <>
TextNode<char>* TextNode<char>::traverse(const std::basic_string<char>& str) const {
	size_t len = str.length();
	if (len == 0) {
		return NULL;
	}
	TextNode<char>::Tree::const_iterator ci = tree.find(str[0]);
	if (ci == tree.end()) {
		return NULL; 
	}
	else {
		if (len == 1) {
			return ci->second;
		}
		else if (ci->second == nullptr) {
			return NULL;
		}
		else {
			return ci->second->traverse(str.substr(1, len - 1));
		}
	}
}

template <typename charT>
TextNode<charT>::TextNode(const std::basic_string<charT>& str, const size_t depth) : tree(), weight(0), depth(depth) {
	add(str);
}

template <typename charT>
TextNode<charT>::TextNode(const size_t depth) : tree(), weight(1), depth(depth) {
}

template <typename charT>
TextNode<charT>::~TextNode() {
	for (typename TextNode<charT>::Tree::iterator it = tree.begin(); it != tree.end(); ++it) {
		delete it->second;
	}
}

template <typename charT>
void TextNode<charT>::add(const std::basic_string<charT>& str) {
	size_t len = str.length();
	typename Tree::iterator it = tree.find(str[0]);
	if (it != tree.end()) {
		if (len == 1) {
			it->second->weight++; 
		}
		else {
			it->second->add(str.substr(1, len - 1));
		}
	}
	else {
		if (len == 1) {
			tree[str[0]] = new TextNode<charT>(depth + 1);
		}
		else {
			tree[str[0]] = new TextNode<charT>(str.substr(1, len - 1), depth + 1);
		}
	}
}

template <typename charT>
void TextNode<charT>::remove(const std::basic_string<charT>& str) {
	typename Tree::iterator it = tree.find(str[0]);
	if (it != tree.end()) {
		size_t len = str.length();
		if (len == 1) {
			if (it->second->weight) { 
				it->second->weight--;
			}
		}
		else {
			it->second->remove(str.substr(1, len - 1));
		}
	}
}

template <typename charT>
void TextNode<charT>::write(std::ofstream& fh) const {
	fh << weight << '[';
	for (typename Tree::const_iterator ci = tree.begin(); ci != tree.end(); ++ci) {
		fh << ci->first;
		ci->second->write(fh);
	}
	fh << ']';
}

template <typename charT>
void TextNode<charT>::read(std::ifstream& fh, size_t depth) {
	char ch;
	this->depth = depth;
	fh >> weight;
	fh >> ch;
	if (!fh.good()) {
		return;
	}
	if (ch != '[') {
		throw std::runtime_error("corrupt text tree save");
	}
	while (fh.good()) {
		fh >> ch;
		if (ch == ']') {
			return;
		}
		tree[ch] = new TextNode<charT>();
		tree[ch]->read(fh, depth + 1);
	}
}

template <typename charT>
void TextTree<charT>::read(const std::string& filename) {
	this->filename = filename;
	tree = new TextNode<charT>;
	if (boost::filesystem::exists(filename)) {
		std::ifstream fh(filename);
		tree->read(fh);
		fh.close();
	}
}

template <typename charT>
TextTree<charT>::~TextTree() {
	if (tree != nullptr) {
		std::ofstream fh(filename);
		tree->write(fh);
		fh.close();
		delete tree;
	}
}

template class TextNode<char>;
//template class TextNode<wchar_t>;
template class TextTree<char>;
//template class TextTree<wchar_t>;
}

#endif
