/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#ifndef HUBGRAPH_TEXT_H
#define HUBGRAPH_TEXT_H

#include "libstemmer.h"
#include <string>
#include <map>
#include <utility>
#include <vector>
#include <unicode/utypes.h>
#include <unicode/unistr.h>
#include <unicode/translit.h>

#include "text_tree.h"

namespace HubGraph {

class Stemmer
{
public:
	Stemmer() : reverse() {}
	auto operator()(const std::string& str) -> std::string;
	auto unstem(const std::string& str) const -> std::string;
	auto unstem(const std::vector<std::string>& str) const -> std::string;
private:
	static sb_stemmer* engStem;
	std::map<const std::string, std::map<const std::string, size_t>> reverse;
};

class Text 
{
public:
	typedef std::pair<std::vector<std::string>, double> WeightedPhrase;
	typedef std::vector<WeightedPhrase> WeightedPhraseVec;
	// NB: text is expected to already be lower-cased
	static auto getSummary(const std::string& text, bool removing = false, int phraseLen = -1, double lenFactor = -1, double proximityFactor = -1, double leadingFactor = -1) -> WeightedPhraseVec;
	static auto getSummary(const std::vector<std::string>& words, bool removing = false, int phraseLen = -1, double lenFactor = -1, double proximityFactor = -1, double leadingFactor = -1) -> WeightedPhraseVec;
	static auto isStopWord(const std::string& word) -> bool;
	static auto getLowercase(std::string text) -> std::string;
	static auto unstem(const std::string& str) -> std::string {
		return stem.unstem(str);
	}
	static auto unstem(const std::vector<std::string>& str) -> std::string {
		return stem.unstem(str);
	}
	static auto getStopWordMap() -> std::map<const std::string, bool> { return stopWords; }
	static auto save(std::ofstream& out) -> void;
	static auto load(std::ifstream& in) -> void;
	static auto getWordCount(const std::string& word) -> size_t;
	static auto splitWords(std::string text) -> std::vector<std::string>;
	static auto transliterate(const std::string& text) -> std::string;

	static Stemmer stem;
	static AsciiTextNode* tree;
private:
	static const std::map<const std::string, bool> stopWords;
};

}

#endif
